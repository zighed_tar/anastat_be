#include <stdio.h>
#include <limits.h>
#include <malloc.h>

// 
void fonc(unsigned int nb, int *tab) {
     int *dst;
     int i;
     printf("nb : %u \n", nb);
     printf("taille : %u \n", sizeof(unsigned int)*nb);
     dst = (int *) calloc(sizeof(unsigned int)*nb);
     if (!dst) { printf("Stop  \n") ; return ;}
     for (i=0; i <nb; i++) { dst[i]=tab[i] ;}
}

void main ()
// UINT_MAX = 2^32 - 1  pour une taille de 4
// c-a-d 4294967295

{	int *tab;
        unsigned int nb ;

        //printf(" UINT_MAX %u \n", UINT_MAX);
        printf(" taille de unsigned int %u \n", sizeof(unsigned int));

        printf(" Premier appel OK : nb vaut 4 \n");
        tab = (int *)malloc(sizeof(unsigned int)*2E8);
        fonc(4, tab);

        printf(" Second appel KO : nb vaut 2^30 \n");
        fonc((unsigned int)1<<30, tab);
}

