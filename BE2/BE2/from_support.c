typedef struct _inside { int a; int b; } INSIDE;
typedef struct _outside { int a; char m; INSIDE x; } OUTSIDE;

OUTSIDE out;

void main(void)
{
        unsigned char *ptr = (unsigned char *) &(out);
        INSIDE *p = (INSIDE *)(ptr + sizeof(int) + sizeof(char));
        p->a = 5;
        p->b = 7;
        assert(out.x.a == 5);
        assert(out.x.b == 7);

}


