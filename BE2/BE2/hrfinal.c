#include <memory.h>
#include <stdlib.h>
#include <stdio.h>

typedef enum { true, false } myboolean;


extern void alloc_with_string( char *pch );
void func_bitfield(myboolean bool); 


int *pl;
int test_array( int avalue )
{
  unsigned char i;
  unsigned int nb;
  pl = (int*)malloc(1000);

  if (avalue)
    memset( pl, 7, 0x1000 );
  pl[100] = 100;
  if (avalue) pl[500] = 500;

  for( nb=0,i=0; i<250; i++ )
    if( pl[i] > 7 ) nb++;

  return *pl;
}


int sum_array[10] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
static int val;
void calculate_sum(void)                
{
    int s, i = val;
    for( s = 0; i++; s++ ) s += sum_array[i];
    printf("the sum of array[] is %d\n", s);
}

extern char get_char(void);
char *chArray[] = { "zero", "one", "two", "three", "four",
                    "five", "six", "seven", "eight"};
void translate_digit_to_string(void)    
{
  char s[6];unsigned int i=0;
  while(*chArray[i] != get_char() && i<sizeof(chArray))
    {
      if ( (i % 2) != 0) strcpy(s,chArray[i]);
      i++;
    }
  if (i == 10) strcpy(s,chArray[i-1]);
}

#define Random() rand
void wait_a_random_period_of_time(void) 
{
  int t = time(NULL);
  while( Random() != 0 ) ; /* wait here */
  printf("We waited %d seconds\n", time(NULL) - t);
}

void calculate_square( int n , myboolean bool)
{
  { 
    int j, square;
    
    if (bool) j = 4; 
    switch( n )
      {
      case 1:     j = square = 4; break;
      case 3:     j = square = 9; break;
      case 4:     if (j % 2 == 0) square = 1; break; 
      default:    j = square = 0;
      }
    if (j % 2 == 0) 
      printf("If %d is less than 4, the square of %d is %d\n", n, j, square);
  }
}

void count_vowels( const char *s )
{
    int sum = 0;
    for(;;)
        switch( *s++ )
        {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                sum++; continue;
            default:   continue;
            case '\0': break;
        }
}


void print_string(void)
{
  char *x;                            
  alloc_with_string(x);
}

void swap( int *pa, int *pb ) { const int temp = *pa; *pa = *pb; *pb = temp; }
int indexa; 
void set_indexa(void) { indexa=10; }

int sort_array[10] = { 0, 4, 7, 1, 9, 3, 6, 2, 3, 6 };
void quick_sort_array( )
{
    int i,j;  
    set_indexa();
    for( i = 0; i < sort_array[indexa]; i++ )
        for( j = i; j > 0; j-- )   /* loop invariant:  array[0:i-1] are already sorted */
            if( sort_array[j] < sort_array[j-1] )
                swap( &sort_array[j], &sort_array[j-1]);
}

void print_list(void)
{
  int arr[3],i;
  for( indexa = 10,i = 0; i < 10; i++ )
    for( arr[0] = 0; arr[0] < 10; arr[0]++ )
      for (arr[1] = 0; arr[1] <10; arr[1]++)
        {
          if( 0 == arr[0] && i == 0 ) break;
          arr[2] = i % arr[0];
        }
}

double double_array[5] = { 0.1, 0.2, 0.3, 0.4, 0.5 };

void print_sum( void )
{
  int i;
  double sum = 0.0;  
  
  for( i=0; i<5; i++ ) sum += double_array[i]; 
  printf( "Sum = %g\n", sum );
}

typedef struct {

  unsigned long var;
} RawBand;
RawBand arrayRaw[12]; 

void copy_raw(void)
{
  unsigned int i; 
  char NofCopy[8] = "NofCopy";
  
  set_indexa();
  for (i = 0 ; i < indexa ; i++)  { 
    memcpy(&(arrayRaw[i].var), NofCopy, i*sizeof(NofCopy[i]));
  }
}

enum tBit { ZERO = 0x00, ONE = 0x01 , TWO = 0x02 };
typedef enum tBit bit;
struct twoBit
{ bit mybit:2; } mybfield;

void func_bitfield(myboolean bool) 
{
  if(bool == false)
    mybfield.mybit =  TWO;
}
