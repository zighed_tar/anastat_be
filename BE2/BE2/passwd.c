#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MSIZE 256

int main(void)
{
	char *login , *passwords;

	login = (char *) malloc(MSIZE);

	strcpy(login, "Albus");

	free(login); // login is now a dangling pointer


	passwords = (char *) malloc(MSIZE);
	// may re - allocate memory area used by login

	strcpy(passwords, "Alohomora");

	printf("%s\n", login); // prints the passwords !

	login[0] = '\0';
	return EXIT_SUCCESS;
}
